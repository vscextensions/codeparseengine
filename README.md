# Readme

API for source code transform using [Roslyn](https://github.com/dotnet/roslyn). Will be used as dependency for [SpaceEngineers Helper](https://gitlab.com/vscextensions/SpaceEngineersHelper)

## Commands
Contains one command, that returns CommandBlockFile build status, and, dependingly on arguments its content.

### Arguments:

```
  -s, --scriptsFolder    Required. Scripts folder path

  -m, --mainFilePath     (Group: FilePathGroup) Main file path

  -f, --mainFilePathR    (Group: FilePathGroup) Main file relative to script folder root path

  -o, --output           Output file path

  -r, --rewrite          Allow rewrite output file

  --comments             Enable comments

  --help                 Display this help screen.

  --version              Display version information.
```

> At least one argument of group must be specified. Which means here that either **-m** or **-f** must be presented.

### Command sample
Note that .NET runtime must be installed, and command line must be running at the same folder where dll file is, otherwise you need specify full path to **CodeParser.dll** file.

```
dotnet CodeParser.dll --scriptsFolder "d:\SpaceEngineers\Scripts" --mainFilePath "d:\SpaceEngineers\Scripts\MessageTransmission.cs" --output "d:\SpaceEngineers\out\MessageTransmission.txt" --rewrite
```
## Dependencies

- .NET 6 or 8 installed.

## Version 1.1.0 [Release]
.NET 8 support added.

## Version 1.0.0 [Release]
Command block building command implemeted.
Parser builds file using only crucial dependencies, in comparison to old TS base implemetation.
