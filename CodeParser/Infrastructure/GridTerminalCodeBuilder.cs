﻿using CodeParser.Engine.Exceptions;
using CodeParser.Engine.Infrastructure;
using CodeParser.Engine.Models;
using CodeParser.Engine.Models.ClassMembers.Extensions;
using CodeParser.Engine.Models.Definitions;
using CodeParser.Engine.Models.Definitions.Interfaces;
using CodeParser.Utils;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using System.Text;
using CodeParser.Exceptions;

namespace CodeParser.Infrastructure
{
    internal class GridTerminalCodeBuilder
    {
        private const string _mainClassName = "Program";
        private readonly string _mainFilePath;
        private readonly string _scriptsDirPath;

        private CodeFileData? _mainFileData;

        public GridTerminalCodeBuilder(string mainFilePath, string scriptsDirPath)
        {
            _mainFilePath = mainFilePath;
            _scriptsDirPath = scriptsDirPath;
        }

        public async Task<CodeFileData> GetMainFileDataAsync()
        {
            if (_mainFileData is null)
            {
                _mainFileData = await FileUtils.GetCodeFileDataAsync(_mainFilePath);
            }

            return _mainFileData;
        }

        public async Task<IList<FileNode>> BuildFilesNodesAsync()
        {
            var filePaths = FileUtils.GetDirFiles(_scriptsDirPath);
            var filesNodes = new List<FileNode>();

            foreach (var filePath in filePaths)
            {
                var fileData = await FileUtils.GetCodeFileDataAsync(filePath);
                var node = await TreeHelper.ToFileNode(fileData);
                filesNodes.Add(node);
            }

            return filesNodes;
        }

        public async Task<Dictionary<string, NamespaceData>> BuildNamespacesDictionaryAsync()
        {
            var fileNodes = await BuildFilesNodesAsync();
            var namespacesDictionary = new Dictionary<string, NamespaceData>();

            foreach (var fileNode in fileNodes)
            {
                if (fileNode.Namespaces is null)
                    continue;

                foreach (var ns in fileNode.Namespaces)
                {
                    var nodeData = new NamespaceNodeData()
                    {
                        FileNode = fileNode,
                        Node = ns.Value
                    };

                    if (namespacesDictionary.ContainsKey(ns.Key))
                    {
                        namespacesDictionary[ns.Key].NodesDatas.Add(nodeData);
                        continue;
                    }

                    namespacesDictionary[ns.Key] = new NamespaceData()
                    {
                        Name = ns.Key,
                        NodesDatas = new List<NamespaceNodeData>() { nodeData }
                    };
                }
            }

            return namespacesDictionary;
        }

        public async Task<string> DefinitionToTextAsync(INodeDefinition nodeDefinition)
        {
            var span = nodeDefinition.GetContentSpan();
            var fileNode = nodeDefinition.FileInNode;

            if (fileNode is null || fileNode.FileData is null)
            {
                ParserEngineException.ThrowFileException(string.Empty, "Node file node not defined, but must be");
                return null!;
            }

            return await fileNode.FileData.GetContentAsync(span);
        }

        public async Task<string> BuildDefinitionsTextAsync(IDictionary<string, NamespaceData> namespacesDic, ClassNode mainClass, FileNode mainFile)
        {
            var index = 0;
            var defs = FindDefinitions(namespacesDic, mainClass, mainFile);
            var tasks = new Task<string>[defs.Count];

            foreach (var def in defs)
            {
                tasks[index] = DefinitionToTextAsync(def);
                index++;
            }

            await Task.WhenAll(tasks.ToArray());

            var sb = new StringBuilder();

            foreach (var task in tasks)
                sb.AppendLine(task.Result);

            return sb.ToString();
        }

        public async Task<string> BuildTerminalCodeTextAsync(bool commentariesEnabled)
        {
            var mainFile = await GetMainFileNodeAsync();

            if (mainFile is null || mainFile.FileData is null)
            {
                CodeParsingApiException.ThrowCommandExecutionException(nameof(BuildTerminalCodeTextAsync), "File data was NULL! Command FAILED");
                return null!;
            }

            var mainClass = await GetMainClassAsync();
            var namespacesDic = await BuildNamespacesDictionaryAsync();

            var mainClassCodeText = await mainFile.FileData.GetContentAsync(mainClass.GetContentSpan());

            var sb = new StringBuilder();

            if (commentariesEnabled)
                sb.Append("// Program class content START\n\n");

            sb.AppendLine(mainClassCodeText);

            if (commentariesEnabled)
                sb.AppendLine("\n// Program class content END\n");

            var tree = BuildDepsTree(namespacesDic, mainClass, mainFile);
            sb.AppendLine(tree.ToString());

            return sb.ToString();
        }

        public SyntaxTree BuildDepsTree(IDictionary<string, NamespaceData> namespacesDic, ClassNode mainClass, FileNode mainFile)
        {
            var defs = FindDefinitions(namespacesDic, mainClass, mainFile);

            var compilationUnit = SyntaxFactory.CompilationUnit();
            compilationUnit = compilationUnit.
                AddMembers(TreeHelper.GetMembers(defs)).
                NormalizeWhitespace();

            return SyntaxFactory.SyntaxTree(compilationUnit);
        }

        public IList<INodeDefinition> FindDefinitions(IDictionary<string, NamespaceData> namespacesDic, ClassNode mainClass, FileNode mainFile)
        {
            var reqTypes = mainClass.GetUsedTypes();
            var reqNamespaces = new SyntaxElementsCollection<IDependencyType>(mainFile.Usings!);
            reqNamespaces.AddElements(mainFile.Namespaces.Values);

            var definitions = new List<INodeDefinition>();

            var initialDefinitions = new DefinitionsCollection()
            {
                RequiredTypes = reqTypes,
                Usings = reqNamespaces
            };

            var maxIterations = namespacesDic.Sum(it => it.Value.NodesDatas.Count);
            var iteration = 0;

            while (iteration <= maxIterations)
            {
                iteration++;
                var foundDefinitionsCollection = GetDefintionsForTypes(namespacesDic, initialDefinitions);

                var foundRequiredTypesCount = reqTypes.Merge(foundDefinitionsCollection.RequiredTypes);
                var foundDefinitionsCount = initialDefinitions.Usings.Merge(foundDefinitionsCollection.Usings);

                initialDefinitions.Definitions.Merge(foundDefinitionsCollection.Definitions);
                var foundDepsCount = reqTypes.TryRemove(foundDefinitionsCollection.Definitions.GetElements().Select(it => it.Type));

                EnvironmentUtils.Log($"{iteration} found: " +
                    $"definitions = {foundDefinitionsCount}, " +
                    $"reqTypesToFind = {foundRequiredTypesCount}, " +
                    $"reqTypesFound = {foundDepsCount}");

                if (foundRequiredTypesCount == 0)
                    break;
            }

            return initialDefinitions.Definitions.GetElements().ToList();
        }

        public DefinitionsCollection GetDefintionsForTypes(IDictionary<string, NamespaceData> namespacesDic,
            in DefinitionsCollection reqDefinitionsCollection)
        {
            var definitionsCollection = new DefinitionsCollection();

            foreach (var usingNode in reqDefinitionsCollection.Usings.GetElements())
            {
                var usingName = usingNode.Name;

                if (!namespacesDic.TryGetValue(usingName, out var namespaceData))
                {
                    EnvironmentUtils.Log($"Namespace {usingName} not found");
                    continue;
                }

                if (namespaceData is null)
                    continue;

                foreach (var namespaceNodeData in namespaceData.NodesDatas)
                {
                    var reqDefinitions = TreeHelper.GetRequiredDefinitions(namespaceNodeData, reqDefinitionsCollection.RequiredTypes);

                    if (reqDefinitions is null || !reqDefinitions.Any())
                    {
                        EnvironmentUtils.Log($"Namespace {usingName} NOT CONTAINS relevant definitions");
                        continue;
                    }

                    EnvironmentUtils.Log($"Namespace {usingName} contained VALID definitions");
                    definitionsCollection.Definitions.AddElements(reqDefinitions);

                    var newTypes = TreeHelper.GetDefinitionsReqTypes(reqDefinitions);
                    definitionsCollection.RequiredTypes.Merge(newTypes);

                    var newNamespaces = namespaceNodeData.FileNode.Usings;
                    if (newNamespaces is not null)
                        definitionsCollection.Usings.AddElements(newNamespaces);
                }

            }

            return definitionsCollection;
        }

        public async Task<ClassNode> GetMainClassAsync()
        {
            var mainFileData = await GetMainFileDataAsync();
            var classes = await mainFileData.GetClassesAsync();
            var classNode = classes.FirstOrDefault(it => it.Name == _mainClassName);

            if (classNode is null)
            {
                CodeParsingApiException.ThrowCommandExecutionException($"Class with name \"{_mainClassName}\" SHOULD be presented in file, " +
                    $"but was not found.");
                return null!;
            }

            return classNode;
        }

        public async Task<FileNode> GetMainFileNodeAsync()
        {
            var mainFileData = await GetMainFileDataAsync();
            return await TreeHelper.ToFileNode(mainFileData);
        }
    }
}
