﻿namespace CodeParser.Exceptions
{
    internal class CodeParsingApiException : Exception
    {
        private CodeParsingApiException(string message) : base(message)
        {

        }

        public static void ThrowCommandExecutionException(string commandName, string description) =>
            throw new CodeParsingApiException($"Execution of {commandName} leaded to an issue: {description}");

        public static void ThrowCommandExecutionException(string description) =>
            throw new CodeParsingApiException($"Execution failed: {description}");
    }
}
