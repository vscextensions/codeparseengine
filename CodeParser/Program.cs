﻿using CodeParser.Infrastructure;
using CodeParser.Utils;
using CommandLine;

namespace CodeParser
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //var scriptsPath = @"D:\code\own\Pets\SpaceEnginerrs\SpaceEngineersHelperExtension\testProject\Scripts\";
            //var files = Directory.GetFiles(scriptsPath, "*.cs", SearchOption.AllDirectories);
            //var mainFilePath = $"{scriptsPath}StaticDepsTest.cs";

            //var builder = new GridTerminalCodeBuilder(mainFilePath, scriptsPath);
            //var text = builder.BuildTerminalCodeTextAsync(false);
            //Task.WaitAll(text);
            //Console.WriteLine();
            //Console.WriteLine(text.Result);

            Parser.Default.ParseArguments<Options>(args)
            .WithParsed(options =>
            {
                try
                {
                    var task = Task.Run(async () => await ExecuteCommand(options));
                    task.Wait();
                    Console.WriteLine(SerializationUtils.Serialize(task.Result));
                }
                catch (Exception ex)
                {
                    var baseException = ex.GetBaseException();
                    var desc = baseException?.Message;

                    if (EnvironmentUtils.IsDebug())
                        desc += $"// {ex.StackTrace}";

                    var response = ApiResponse.CreateFailedResponse(desc);

                    Console.WriteLine(SerializationUtils.Serialize(response));
                }
            });
        }

        private static async Task<ApiResponse> ExecuteCommand(Options options)
        {
            string mainFilePath = options.MainFilePath;

            if (mainFilePath is null)
                mainFilePath = options.ScriptsFolderPath + options.MainFileRelativePath;

            var builder = new GridTerminalCodeBuilder(mainFilePath, options.ScriptsFolderPath);
            var fileText = builder.BuildTerminalCodeTextAsync(options.CommentariesEnabled);

            if (options.OutputFilePath is null)
                return ApiResponse.CreateSuccessResponse(fileText.Result);

            await FileUtils.WriteToFileAsync(options.OutputFilePath, fileText.Result, overWrite: options.AllowRewriteOutput);
            return ApiResponse.CreateSuccessResponse($"Written to file: {options.OutputFilePath}", null);
        }
    }
}