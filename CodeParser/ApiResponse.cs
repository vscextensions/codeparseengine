﻿using System.Text.Json.Serialization;

namespace CodeParser
{
    public enum ResponseStatus
    {
        Success,
        Failed
    }

    internal class ApiResponse
    {
        [JsonPropertyName("status")]
        public ResponseStatus Status { get; set; }

        [JsonPropertyName("description")]
        public string? Description { get; set; }

        [JsonPropertyName("payload")]
        public string? Payload { get; set; }

        private ApiResponse(ResponseStatus status, string? description)
        {
            Status = status;
            Description = description;
        }

        public static ApiResponse CreateSuccessResponse(string? description, string? payload)
        {
            var response = new ApiResponse(ResponseStatus.Success, description);
            response.Payload = payload;

            return response;
        }
        public static ApiResponse CreateSuccessResponse(string? payload)
        {
            var response = new ApiResponse(ResponseStatus.Success, null);
            response.Payload = payload;

            return response;
        }

        public static ApiResponse CreateFailedResponse(string? description)
        {
            var response = new ApiResponse(ResponseStatus.Failed, description);

            return response;
        }
    }
}
