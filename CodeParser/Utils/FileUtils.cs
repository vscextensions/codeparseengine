﻿using CodeParser.Engine.Exceptions;
using CodeParser.Engine.Infrastructure;
using CodeParser.Engine.Models;

namespace CodeParser.Utils
{
    internal class FileUtils
    {
        public static async Task<string?> GetTextFileContentAsync(string path)
        {
            if (!File.Exists(path))
            {
                ParserEngineException.ThrowFileException(path, "File not found");
                return null;
            }

            return await File.ReadAllTextAsync(path);
        }

        public static async Task WriteToFileAsync(string path, string content, CancellationToken ct = default, bool overWrite = false)
        {
            var isExist = File.Exists(path);

            if (!overWrite && isExist)
            {
                ParserEngineException.ThrowFileException(path, "File already exists");
                return;
            }

            if(isExist)
                File.Delete(path);

            var dirPath = Path.GetDirectoryName(path);

            if(dirPath is not null && !Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            await File.WriteAllTextAsync(path, content, ct);
        }

        public static string[] GetDirFiles(string path)
        {
            return Directory.GetFiles(path, "*.cs", SearchOption.AllDirectories);
        }

        public static async Task<CodeFileData> GetCodeFileDataAsync(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                ParserEngineException.ThrowFileException("*NO PATH*", "Path was empty");
                return null!;
            }

            var fileContent = await GetTextFileContentAsync(path);

            if (fileContent is null)
            {
                ParserEngineException.ThrowFileException(path, "Returned no content");
                return null!;
            }

            return new CodeFileData(path, TreeHelper.GetTree(fileContent));
        }
    }
}
