﻿using System.Reflection.Metadata.Ecma335;

namespace CodeParser.Utils
{
    internal class EnvironmentUtils
    {
        public static bool IsDebug()
        {
#if !DEBUG
                return false;
#endif

            return true;
        }

        public static void Log(string message)
        {
            if (IsDebug())
                Console.WriteLine(message);
        }
    }
}
