﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace CodeParser.Utils
{
    internal class SerializationUtils
    {
        public static string Serialize<T>(T obj)
        {
            var options = new JsonSerializerOptions();
            options.Converters.Add(new JsonStringEnumConverter());

            return JsonSerializer.Serialize(obj, options);
        }
    }
}
