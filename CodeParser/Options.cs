﻿using CommandLine;

namespace CodeParser
{
    public class Options
    {
        private const string FilePathGroupName = "FilePathGroup";

        [Option('s', "scriptsFolder", Required = true, HelpText = "Scripts folder path")]
        public string ScriptsFolderPath { get; set; } = null!;

        [Option('m', "mainFilePath", HelpText = "Main file path", Group = FilePathGroupName)]
        public string MainFilePath { get; set; } = null!;

        [Option('f', "mainFilePathR", HelpText = "Main file relative to script folder root path", Group = FilePathGroupName)]
        public string MainFileRelativePath { get; set; } = null!;

        [Option('o', "output", HelpText = "Output file path")]
        public string OutputFilePath { get; set; } = null!;

        [Option('r', "rewrite", HelpText = "Allow rewrite output file")]
        public bool AllowRewriteOutput { get; set; } = false;

        [Option("comments", HelpText = "Enable comments")]
        public bool CommentariesEnabled { get; set; }
    }
}
