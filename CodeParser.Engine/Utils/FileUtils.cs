﻿using CodeParser.Engine.Exceptions;
using CodeParser.Engine.Infrastructure;
using CodeParser.Engine.Models;

namespace CodeParser.Engine.Utils
{
    internal class FileUtils
    {
        public static async Task<string?> GetTextFileContent(string path)
        {
            if (!File.Exists(path))
            {
                ParserEngineException.ThrowFileException(path, "File not found");
                return null;
            }
            
            return await File.ReadAllTextAsync(path);
        }

        public static string[] GetDirFiles(string path)
        {
            return Directory.GetFiles(path, "*.cs", SearchOption.AllDirectories);
        }

        public static async Task<CodeFileData> GetCodeFileDataAsync(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                ParserEngineException.ThrowFileException("*NO PATH*", "Path was empty");
                return null!;
            }

            var fileContent = await GetTextFileContent(path);

            if (fileContent is null)
            {
                ParserEngineException.ThrowFileException(path, "Returned no content");
                return null!;
            }

            return new CodeFileData(path, TreeHelper.GetTree(fileContent));
        }
    }
}
