﻿using CodeParser.Engine.Exceptions;
using CodeParser.Engine.Models;
using CodeParser.Engine.Models.ClassMembers.Extensions;
using CodeParser.Engine.Models.Definitions;
using CodeParser.Engine.Models.Definitions.Interfaces;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeParser.Engine.Infrastructure
{
    public class TreeHelper
    {
        public static SyntaxElementsCollection<IDependencyType> GetDefinitionsReqTypes(IList<INodeDefinition> nodeDefinitions)
        {
            var reqTypes = new SyntaxElementsCollection<IDependencyType>();

            foreach (var def in nodeDefinitions)
            {
                if (def is InterfaceNode interfaceNode)
                {
                    reqTypes.Merge(interfaceNode.GetUsedTypes());
                    continue;
                }

                if (def is not IObjectNodeDefinition objectDef)
                    continue;

                reqTypes.Merge(objectDef.GetUsedTypes());
            }

            return reqTypes;
        }

        public static IList<INodeDefinition>? GetRequiredDefinitions(NamespaceNodeData namespaceNodeData, 
            in SyntaxElementsCollection<IDependencyType> reqTypes)
        {
            var allDefinitions = namespaceNodeData.Node.Definitions;

            if (allDefinitions is null || !allDefinitions.Any())
                return null;

            var reqDefinitions = new List<INodeDefinition>();

            foreach (var def in allDefinitions.Values)
            {
                if (!reqTypes.Contains(def.Name))
                    continue;
                
                def.FileInNode = namespaceNodeData.FileNode;
                reqDefinitions.Add(def);
            }

            return reqDefinitions;
        }

        public static async Task<FileNode> ToFileNode(CodeFileData fileData)
        {
            var namespaces = await fileData.GetNamespacesAsync();

            return new FileNode()
            {
                Namespaces = namespaces.ToDictionary(it => it.Name, it => it),
                Usings = fileData.GetUsings().ToList(),
                FileData = fileData,
                Path = fileData.Path
            };
        }

        public static IEnumerable<UsingNode> GetUsings(SyntaxTree syntaxTree)
        {
            CompilationUnitSyntax root = syntaxTree.GetCompilationUnitRoot();

            foreach (var u in root.Usings)
            {
                if (u is null || u.Name is null)
                    continue;

                yield return new UsingNode() { Name = u.Name.ToString(), Type = u.Name.ToString() };
            }
        }

        public static IEnumerable<NamespaceNode> GetNamespaces(SyntaxNode syntaxNode)
        {
            var namespaceNodes = new List<NamespaceNode>();
            
            var namespacesSyntaxes = syntaxNode.DescendantNodes().OfType<NamespaceDeclarationSyntax>();
            var fileScopedNamespaceSyntaxes = syntaxNode.DescendantNodes().OfType<FileScopedNamespaceDeclarationSyntax>();
            
            namespaceNodes.AddRange(namespacesSyntaxes.Select(ns => ns.ToNode()));
            namespaceNodes.AddRange(fileScopedNamespaceSyntaxes.Select(ns => ns.ToNode()));
            
            return namespaceNodes;
        }

        public static IEnumerable<string> GetNamespacesNames(SyntaxNode syntaxNode)
        {
            return GetNamespaces(syntaxNode).Select(it => it.Name);
        }

        public static SyntaxTree GetTree(string codeText)
        {
            var syntaxTree = CSharpSyntaxTree.ParseText(codeText);

            if (syntaxTree is null)
            {
                ParserEngineException.ThrowTreeParsingException("Syntax tree appeared NULL");
                return null!;
            }

            return syntaxTree;
        }

        public static string GetContent(string codeText, ContentSpan span)
        {
            return codeText.Substring(span.Start, span.End - span.Start);
        }

        public static MemberDeclarationSyntax[] GetMembers(IEnumerable<INodeDefinition> definitions)
        {
            var members = new List<MemberDeclarationSyntax>();

            AddRange(FilterDefinitions<EnumDeclarationSyntax>(definitions));
            AddRange(FilterDefinitions<StructDeclarationSyntax>(definitions));
            AddRange(FilterDefinitions<InterfaceDeclarationSyntax>(definitions));
            AddRange(FilterDefinitions<ClassDeclarationSyntax>(definitions));

            return members.ToArray();

            void AddRange(IEnumerable<MemberDeclarationSyntax>? membersToAdd)
            {
                if (membersToAdd is null)
                    return;

                members.AddRange(membersToAdd);
            }
        }

        public static T[]? FilterDefinitions<T>(IEnumerable<INodeDefinition> definitions) where T : class
        {
            var type = typeof(T);
            var filteredDefinitions = definitions.Where(it => it.SyntaxDeclarationType?.FullName == type.FullName).Select(it => it.SyntaxNode as T);

            if (filteredDefinitions is null || !filteredDefinitions.Any())
                return null;

            return filteredDefinitions.ToArray()!;
        }
    }
}
