﻿namespace CodeParser.Engine.Infrastructure
{
    public static class SyntaxConstants
    {
        public static readonly HashSet<string> AccessModifiersToRemove = new HashSet<string>
        {
            "internal",
            "public"
        };
    }
}
