﻿using CodeParser.Engine.Models;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeParser.Engine.Infrastructure
{
    public class NodesHelper
    {
        public static ContentSpan? GetContentSpan(SyntaxNode? syntaxNode)
        {
            var classDeclaration = syntaxNode as TypeDeclarationSyntax;
            var members = classDeclaration?.Members;

            return GetMembersSpan(members);
        }

        public static ContentSpan? GetMembersSpan(SyntaxList<MemberDeclarationSyntax>? members)
        {
            var span = new ContentSpan();

            if (members is null || !members.Value.Any())
                return null;

            var firstMember = members.Value.First();
            var lastMember = members.Value.Last();

            span.Start = firstMember.Span.Start;
            span.End = lastMember.Span.End;

            return span;
        }
    }
}
