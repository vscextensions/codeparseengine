﻿namespace CodeParser.Engine.Models
{
    public class FileNode
    {
        public IList<UsingNode>? Usings { get; set; }
        public IDictionary<string, NamespaceNode> Namespaces { get; set; } = null!;
        public CodeFileData? FileData { get; set; }
        public string Path { get; set; } = string.Empty;
    }
}
