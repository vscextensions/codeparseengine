﻿namespace CodeParser.Engine.Models
{
    public class NamespaceData
    {
        public string Name { get; set; } = null!;
        public IList<NamespaceNodeData> NodesDatas { get; set; } = null!;
    }

    public class NamespaceNodeData
    {
        public FileNode FileNode { get; set; } = null!;
        public NamespaceNode Node { get; set; } = null!;
    }
}
