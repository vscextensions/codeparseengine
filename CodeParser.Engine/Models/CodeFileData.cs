﻿using CodeParser.Engine.Exceptions;
using CodeParser.Engine.Infrastructure;
using CodeParser.Engine.Models.ClassMembers.Extensions;
using CodeParser.Engine.Models.Definitions;
using CodeParser.Engine.Models.Definitions.Interfaces;
using CodeParser.Engine.Utils;
using Microsoft.CodeAnalysis;

namespace CodeParser.Engine.Models
{
    public class CodeFileData
    {
        private SyntaxNode? _rootNode;
        private string? _codeText = null;
        private static SemaphoreSlim _sem = new SemaphoreSlim(1, 1);

        public SyntaxTree SyntaxTree { get; set; }
        public string Path { get; }


        public CodeFileData(string path, SyntaxTree syntaxTree)
        {
            Path = path;
            SyntaxTree = syntaxTree;
        }

        public async Task<SyntaxNode> GetRootNodeAsync()
        {
            if (_rootNode is null)
                _rootNode = await SyntaxTree.GetRootAsync();

            return _rootNode;
        }

        public IEnumerable<UsingNode> GetUsings() => TreeHelper.GetUsings(SyntaxTree);
        public async Task<IEnumerable<string>> GetNamespacesNamesAsync() => TreeHelper.GetNamespacesNames(await GetRootNodeAsync());
        public async Task<IEnumerable<NamespaceNode>> GetNamespacesAsync() => TreeHelper.GetNamespaces(await GetRootNodeAsync());
        public async Task<IList<ClassNode>> GetClassesAsync()
        {
            var node = await GetRootNodeAsync();
            return node.GetAllClasses();
        }
        public async Task<IList<StructNode>> GetStructsAsync()
        {
            var node = await GetRootNodeAsync();
            return node.GetAllStructs();
        }
        public async Task<IList<EnumNode>> GetEnumsAsync()
        {
            var node = await GetRootNodeAsync();
            return node.GetAllEnums();
        }
        public async Task<IList<INodeDefinition>> GetDefinitionsAsync()
        {
            var definitions = new List<INodeDefinition>();

            definitions.AddRange(await GetClassesAsync());
            definitions.AddRange(await GetStructsAsync());
            definitions.AddRange(await GetEnumsAsync());

            return definitions;
        }
        public async Task<string> GetContentAsync(ContentSpan? span = null)
        {
            try
            {
                await _sem.WaitAsync();

                if (_codeText is null)
                    _codeText = await FileUtils.GetTextFileContent(Path);

                var node = await GetRootNodeAsync();

                if (span is null)
                {
                    span = new ContentSpan()
                    {
                        Start = node.Span.Start,
                        End = node.Span.End
                    };
                }
            }
            finally
            {
                _sem.Release();
            }

            if (_codeText is not null)
                return TreeHelper.GetContent(_codeText, span);

            ParserEngineException.ThrowFileException(Path, "Content was null, but must be value");
            return null!;
        }
    }
}
