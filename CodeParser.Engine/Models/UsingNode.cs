﻿namespace CodeParser.Engine.Models
{
    public class UsingNode: IDependencyType
    {
        public string Name { get; set; } = null!;
        public string Type { get; set; } = null!;
    }
}
