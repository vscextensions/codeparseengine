﻿namespace CodeParser.Engine.Models
{
    public class TypeInfo : IDependencyType
    {
        public string Name { get; set; } = null!;
        public string Type { get; set; } = null!;
        public bool IsFound { get; set; }
    }

    public class SyntaxElementsCollection<T> where T : IDependencyType
    {
        protected IDictionary<string, T> _elements = new Dictionary<string, T>();

        public SyntaxElementsCollection()
        {

        }

        public SyntaxElementsCollection(IEnumerable<T> elements)
        {
            AddElements(elements);
        }

        public IEnumerable<T> GetElements() => _elements.Values;

        public bool Contains(string key) => _elements.ContainsKey(key);

        public int AddElements(IEnumerable<T>? infos)
        {
            var count = 0;

            if (infos is null)
                return count;

            foreach (var info in infos)
            {
                if (TryAddElement(info))
                    count++;
            }

            return count;
        }

        public bool TryAddElement(T typeInfo)
        {
            if (typeInfo is null)
                return false;

            return _elements!.TryAdd(typeInfo.Type, typeInfo);
        }

        public int Merge(SyntaxElementsCollection<T> collection)
        {
            var elements = collection.GetElements();
            return AddElements(elements);
        }

        public bool TryRemove(string key) => _elements.Remove(key);
        public int TryRemove(IEnumerable<string> keys)
        {
            var count = 0;

            foreach (var key in keys)
                if(TryRemove(key))
                    count++;

            return count;
        }
    }
}
