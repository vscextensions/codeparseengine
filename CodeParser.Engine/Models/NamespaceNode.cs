﻿using CodeParser.Engine.Models.Definitions.Interfaces;
using Microsoft.CodeAnalysis;

namespace CodeParser.Engine.Models
{
    public class NamespaceNode: INodeDefinition
    {
        public string Name { get; set; } = null!;
        public string Type => Name;
        public ContentSpan? Span { get; set; }
        public IDictionary<string, INodeDefinition>? Definitions { get; set; }
        public SyntaxNode? SyntaxNode { get; set; }
        public Type? SyntaxDeclarationType { get; set; }
        public FileNode? FileInNode { get; set; }
    }
}
