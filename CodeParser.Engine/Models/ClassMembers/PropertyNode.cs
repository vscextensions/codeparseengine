﻿namespace CodeParser.Engine.Models.ClassMembers
{
    public class PropertyNode: IDependencyType
    {
        public string Type { get; set; } = null!;
        public string Name { get; set; } = null!;
    }
}
