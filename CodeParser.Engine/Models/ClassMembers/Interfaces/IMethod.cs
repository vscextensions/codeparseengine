﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeParser.Engine.Models.ClassMembers.Interfaces
{
    public interface IMethod
    {
        List<VariableNode>? InputVars { get; set; }
        List<VariableNode>? UsedVars { get; set; }
    }
}
