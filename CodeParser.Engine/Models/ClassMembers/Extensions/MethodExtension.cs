﻿using CodeParser.Engine.Models.ClassMembers.Interfaces;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeParser.Engine.Models.ClassMembers.Extensions
{
    public static class MethodExtension
    {
        public static void FillUsedTypes(this IMethod method, ParameterListSyntax parameterList, BlockSyntax? body) {
            foreach (var param in parameterList.Parameters)
            {
                method.InputVars!.Add(new VariableNode()
                {
                    Name = param.Identifier.ToString(),
                    Type = param.Type!.ToString()
                });
            }

            if (body == null) return;

            var descNodes = body.DescendantNodes();
            var createdObjs = descNodes.OfType<ObjectCreationExpressionSyntax>();
            foreach (var obj in createdObjs)
            {
                method.UsedVars!.Add(new VariableNode() { Type = obj.Type.ToString() });
            }

            var calledStatics = descNodes.OfType<MemberAccessExpressionSyntax>();

            foreach (var calledStatic in calledStatics)
            {
                if (calledStatic != null)
                {
                    var className = calledStatic.Expression.ToString();
                    method.UsedVars!.Add(new VariableNode() { Type = className });
                }
            }
        }
    }
}
