﻿namespace CodeParser.Engine.Models.ClassMembers.Extensions
{
    public static class CollectionsExtensions
    {
        public static int AddRange<T>(this HashSet<T> set, IEnumerable<T> values)
        {
            var count = 0;

            foreach (var value in values)
            {
                if (set.Add(value))
                    count++; 
            }

            return count;
        }
    }
}
