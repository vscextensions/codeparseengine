﻿using CodeParser.Engine.Models.Definitions;
using CodeParser.Engine.Models.Definitions.Interfaces;

namespace CodeParser.Engine.Models.ClassMembers.Extensions
{
    public static class ClassExtensions
    {
        public static SyntaxElementsCollection<IDependencyType> GetUsedTypes(this InterfaceNode node)
        {
            var types = new SyntaxElementsCollection<IDependencyType>();

            types.AddElements(node.Props);
            types.AddElements(node.BaseTypes);

            var methods = node.Methods;

            if (methods is not null)
            {
                foreach (var m in methods)
                {
                    types.AddElements(m.InputVars);
                    types.AddElements(m.UsedVars);
                    types.TryAddElement(m.OutputType);
                }
            }

            return types;
        }

        public static SyntaxElementsCollection<IDependencyType> GetUsedTypes(this IObjectNodeDefinition classNode)
        {
            var types = new SyntaxElementsCollection<IDependencyType>();

            types.AddElements(classNode.Vars);
            types.AddElements(classNode.Props);
            types.AddElements(classNode.BaseTypes);

            var methods = classNode.Methods;

            if (methods is not null)
            {
                foreach (var m in methods)
                {
                    types.AddElements(m.InputVars);
                    types.AddElements(m.UsedVars);
                    types.TryAddElement(m.OutputType);
                }
            }

            var ctors = classNode.Ctors;

            if (ctors is not null)
            {
                foreach (var c in ctors)
                {
                    types.AddElements(c.InputVars);
                    types.AddElements(c.UsedVars);
                }
            }

            return types;
        }
    }
}
