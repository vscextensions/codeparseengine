﻿using CodeParser.Engine.Models.Definitions;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis;
using CodeParser.Engine.Models.Definitions.Interfaces;

namespace CodeParser.Engine.Models.ClassMembers.Extensions
{
    public static class SyntaxNodeExtensions
    {
        public static ContentSpan? GetContentSpan(this INodeDefinition nodeDefinition)
        {
            if (nodeDefinition.SyntaxNode is null)
                return null;

            return ContentSpan.Convert(nodeDefinition.SyntaxNode.Span);
        }
        public static List<PropertyNode> GetAllProps(this SyntaxNode rootNode)
        {
            var properties = rootNode.DescendantNodes().OfType<PropertyDeclarationSyntax>();
            var propNodes = new List<PropertyNode>(properties.Count());

            foreach (var property in properties)
                propNodes.Add(property.ToNode());

            return propNodes;
        }

        public static List<VariableNode> GetAllVars(this SyntaxNode rootNode)
        {
            var vars = rootNode.DescendantNodes().OfType<VariableDeclarationSyntax>();
            var propNodes = new List<VariableNode>(vars.Count());

            foreach (var variableDeclaration in vars)
                propNodes.AddRange(variableDeclaration.ToNodes());

            return propNodes;
        }

        public static List<CtorNode> GetAllCtors(this SyntaxNode rootNode)
        {
            var ctors = rootNode.DescendantNodes().OfType<ConstructorDeclarationSyntax>();
            var ctorNodes = new List<CtorNode>(ctors.Count());

            foreach (var ctor in ctors)
                ctorNodes.Add(ctor.ToNode());

            return ctorNodes;
        }

        public static List<MethodNode> GetAllMethods(this SyntaxNode rootNode)
        {
            var methods = rootNode.DescendantNodes().OfType<MethodDeclarationSyntax>();
            var methodsNodes = new List<MethodNode>(methods.Count());

            foreach (var method in methods)
                methodsNodes.Add(method.ToNode());

            return methodsNodes;
        }

        public static List<BaseTypeNode> GetAllBaseTypes(this TypeDeclarationSyntax syntax)
        {
            var nodes = new List<BaseTypeNode>();

            if (syntax.BaseList is null)
                return nodes;

            foreach (var b in syntax.BaseList.Types)
                nodes.Add(b.ToNode());

            return nodes;
        }

        public static List<ClassNode> GetAllClasses(this SyntaxNode rootNode)
        {
            var classes = rootNode.DescendantNodes().OfType<ClassDeclarationSyntax>();
            var classesNodes = new List<ClassNode>(classes.Count());

            foreach (var c in classes)
                classesNodes.Add(c.ToNode());

            return classesNodes;
        }

        public static List<StructNode> GetAllStructs(this SyntaxNode rootNode)
        {
            var structs = rootNode.DescendantNodes().OfType<StructDeclarationSyntax>();
            var classesNodes = new List<StructNode>(structs.Count());

            foreach (var s in structs)
                classesNodes.Add(s.ToNode());

            return classesNodes;
        }

        public static List<EnumNode> GetAllEnums(this SyntaxNode rootNode)
        {
            var structs = rootNode.DescendantNodes().OfType<EnumDeclarationSyntax>();
            var classesNodes = new List<EnumNode>(structs.Count());

            foreach (var e in structs)
                classesNodes.Add(e.ToNode());

            return classesNodes;
        }
    }
}
