﻿using CodeParser.Engine.Infrastructure;
using CodeParser.Engine.Models.Definitions;
using CodeParser.Engine.Models.Definitions.Interfaces;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeParser.Engine.Models.ClassMembers.Extensions
{
    public static class DeclarationExtensions
    {
        public static NamespaceNode ToNode(this NamespaceDeclarationSyntax declarationSyntax)
        {
            return new NamespaceNode()
            {
                Name = declarationSyntax.Name.ToString(),
                Span = NodesHelper.GetMembersSpan(declarationSyntax.Members),
                Definitions = GetDefinitions(declarationSyntax),
            };
        }
        
        private static IDictionary<string, INodeDefinition> GetDefinitions(NamespaceDeclarationSyntax declarationSyntax)
        {
            var definitions = new Dictionary<string, INodeDefinition>();

            foreach (var d in declarationSyntax.Members)
            {
                INodeDefinition? definition = d.ToNodeDefinition();

                if (definition is null)
                    continue;

                definitions.Add(definition.Name, definition);
            }

            return definitions;
        }
        
        public static NamespaceNode ToNode(this FileScopedNamespaceDeclarationSyntax declarationSyntax)
        {
            return new NamespaceNode()
            {
                Name = declarationSyntax.Name.ToString(),
                Span = NodesHelper.GetMembersSpan(declarationSyntax.Members),
                Definitions = GetDefinitions(declarationSyntax),
            };
        }
        
        private static IDictionary<string, INodeDefinition> GetDefinitions(FileScopedNamespaceDeclarationSyntax declarationSyntax)
        {
            var definitions = new Dictionary<string, INodeDefinition>();

            foreach (var d in declarationSyntax.Members)
            {
                INodeDefinition? definition = d.ToNodeDefinition();

                if (definition is null)
                    continue;

                definitions.Add(definition.Name, definition);
            }

            return definitions;
        }

        private static INodeDefinition? ToNodeDefinition(this MemberDeclarationSyntax memberDeclaration)
        {
            INodeDefinition? nodeDefinition = null;

            if (memberDeclaration is ClassDeclarationSyntax classSyntax)
                nodeDefinition = classSyntax.ToNode();
            else if (memberDeclaration is StructDeclarationSyntax structDeclaration)
                nodeDefinition = structDeclaration.ToNode();
            else if (memberDeclaration is EnumDeclarationSyntax enumDeclaration)
                nodeDefinition = enumDeclaration.ToNode();
            else if (memberDeclaration is InterfaceDeclarationSyntax interfaceDeclaration)
                nodeDefinition = interfaceDeclaration.ToNode();

            if (nodeDefinition is not null)
            {
                nodeDefinition.SyntaxNode = memberDeclaration;
                nodeDefinition.SyntaxDeclarationType = memberDeclaration.GetType();
            }

            return nodeDefinition;
        }

        public static InterfaceNode ToNode(this InterfaceDeclarationSyntax c)
        {
            var node = new InterfaceNode();

            node.Name = c.Identifier.ToString();
            node.Props = c.GetAllProps();
            node.Methods = c.GetAllMethods();
            node.BaseTypes = c.GetAllBaseTypes();
            node.SyntaxNode = c;

            return node;
        }

        public static EnumNode ToNode(this EnumDeclarationSyntax declarationSyntax)
        {
            return new EnumNode() { Name = declarationSyntax.Identifier.ToString() };
        }        
        
        public static BaseTypeNode ToNode(this BaseTypeSyntax declarationSyntax)
        {
            return new BaseTypeNode()
            {
                Type = declarationSyntax.Type.ToString(),
            };
        }

        public static StructNode ToNode(this StructDeclarationSyntax c)
        {
            var classNode = new StructNode();

            classNode.Name = c.Identifier.ToString();
            classNode.Props = c.GetAllProps();
            classNode.Vars = c.GetAllVars();
            classNode.Methods = c.GetAllMethods();
            classNode.Ctors = c.GetAllCtors();
            classNode.BaseTypes = c.GetAllBaseTypes();
            classNode.SyntaxNode = c;

            return classNode;
        }

        public static ClassNode ToNode(this ClassDeclarationSyntax c)
        {
            var classNode = new ClassNode();

            classNode.Name = c.Identifier.ToString();
            classNode.Props = c.GetAllProps();
            classNode.Vars = c.GetAllVars();
            classNode.Methods = c.GetAllMethods();
            classNode.Ctors = c.GetAllCtors();
            classNode.BaseTypes = c.GetAllBaseTypes();
            classNode.SyntaxNode = c;

            return classNode;
        }

        public static PropertyNode ToNode(this PropertyDeclarationSyntax declarationSyntax)
        {
            return new PropertyNode()
            {
                Name = declarationSyntax.Identifier.ValueText,
                Type = declarationSyntax.Type.ToString(),
            };
        }
        public static IList<VariableNode> ToNodes(this VariableDeclarationSyntax declarationSyntax)
        {
            var vars = new List<VariableNode>();
            var type = declarationSyntax.Type.ToString();

            foreach (var variable in declarationSyntax.Variables)
            {
                var node = new VariableNode()
                {
                    Name = variable.Identifier.ToString(),
                    Type = type,
                };

                vars.Add(node);
            }

            return vars;
        }

        public static CtorNode ToNode(this ConstructorDeclarationSyntax declarationSyntax)
        {
            var ctorNode = new CtorNode();
            ctorNode.FillUsedTypes(declarationSyntax.ParameterList, declarationSyntax.Body);

            return ctorNode;
        }

        public static MethodNode ToNode(this MethodDeclarationSyntax declarationSyntax)
        {
            var node = new MethodNode();
            node.FillUsedTypes(declarationSyntax.ParameterList, declarationSyntax.Body);
            var returnType = declarationSyntax.ReturnType;
            node.OutputType = new TypeInfo() { Type = declarationSyntax.ReturnType.ToString() };

            return node;
        }
    }
}
