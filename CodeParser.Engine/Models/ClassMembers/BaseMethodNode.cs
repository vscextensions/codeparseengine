﻿namespace CodeParser.Engine.Models.ClassMembers
{
    public abstract class BaseMethodNode
    {
        public List<VariableNode>? InputVars { get; set; } = new List<VariableNode>();
        public List<VariableNode>? UsedVars { get; set; } = new List<VariableNode>();
    }
}
