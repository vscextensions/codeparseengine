﻿namespace CodeParser.Engine.Models.ClassMembers
{
    public class BaseTypeNode : IDependencyType
    {
        public string Name => Type;

        public string Type { get; set; } = null!;
    }
}
