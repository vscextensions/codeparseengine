﻿namespace CodeParser.Engine.Models.ClassMembers
{
    public class VariableNode : IDependencyType
    {
        public string Name { get; set; } = null!;
        public string Type { get; set; } = null!;
    }
}
