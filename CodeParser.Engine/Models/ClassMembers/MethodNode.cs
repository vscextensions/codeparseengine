﻿using CodeParser.Engine.Models.ClassMembers.Interfaces;

namespace CodeParser.Engine.Models.ClassMembers
{
    public class MethodNode: BaseMethodNode, IMethod
    {
        public TypeInfo OutputType { get; set; } = null!;
    }
}
