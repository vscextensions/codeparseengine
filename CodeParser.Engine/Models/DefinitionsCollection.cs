﻿using CodeParser.Engine.Models.Definitions.Interfaces;

namespace CodeParser.Engine.Models
{
    public class DefinitionsCollection
    {
        public SyntaxElementsCollection<IDependencyType> RequiredTypes { get; set; } = new SyntaxElementsCollection<IDependencyType>();
        public SyntaxElementsCollection<INodeDefinition> Definitions { get; set; } = new SyntaxElementsCollection<INodeDefinition>();
        public SyntaxElementsCollection<IDependencyType> Usings { get; set; } = new SyntaxElementsCollection<IDependencyType>();
    }
}
