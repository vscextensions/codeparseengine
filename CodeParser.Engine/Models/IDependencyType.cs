﻿namespace CodeParser.Engine.Models
{
    public interface IDependencyType
    {
        public string Name { get; }
        public string Type { get; }
    }
}