﻿using Microsoft.CodeAnalysis.Text;

namespace CodeParser.Engine.Models
{
    public class ContentSpan
    {
        public int Start { get; set; }
        public int End { get; set; }

        public static ContentSpan Convert(TextSpan textSpan)
        {
            return new ContentSpan()
            {
                Start = textSpan.Start,
                End = textSpan.End,
            };
        }
    }
}
