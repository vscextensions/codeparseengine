﻿using Microsoft.CodeAnalysis;

namespace CodeParser.Engine.Models.Definitions.Interfaces
{
    public interface INodeDefinition: IDependencyType
    {
        SyntaxNode? SyntaxNode { get; set; }
        FileNode? FileInNode { get; set; }
        Type? SyntaxDeclarationType { get; set; }
    }
}
