﻿using CodeParser.Engine.Models.ClassMembers;

namespace CodeParser.Engine.Models.Definitions.Interfaces
{
    public interface IObjectNodeDefinition
    {
        List<CtorNode>? Ctors { get; }
        List<MethodNode>? Methods { get; }
        List<PropertyNode>? Props { get; }
        List<VariableNode>? Vars { get; }
        List<BaseTypeNode>? BaseTypes { get; set; }
    }
}