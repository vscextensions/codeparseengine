﻿using CodeParser.Engine.Infrastructure;
using CodeParser.Engine.Models.ClassMembers;
using CodeParser.Engine.Models.Definitions.Interfaces;
using Microsoft.CodeAnalysis;

namespace CodeParser.Engine.Models.Definitions
{
    public class InterfaceNode : INodeDefinition
    {
        public string Name { get; set; } = null!;
        public string Type => Name;
        public List<BaseTypeNode>? BaseTypes { get; set; }
        public List<MethodNode>? Methods { get; set; }
        public List<PropertyNode>? Props { get; set; }
        public SyntaxNode? SyntaxNode { get; set; }
        public Type? SyntaxDeclarationType { get; set; }
        public FileNode? FileInNode { get; set; }
        public ContentSpan? GetFullSpan()
        {
            if (SyntaxNode is null)
                return null;

            return ContentSpan.Convert(SyntaxNode.FullSpan);
        }
        public ContentSpan? GetContentSpan()
        {
            return NodesHelper.GetContentSpan(SyntaxNode);
        }
    }
}
