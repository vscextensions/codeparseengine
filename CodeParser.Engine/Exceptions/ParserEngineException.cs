﻿namespace CodeParser.Engine.Exceptions
{
    public class ParserEngineException : Exception
    {
        public ParserEngineException(string message) : base(message)
        {

        }

        public static void ThrowFileException(string fileName, string description) =>
            throw new ParserEngineException($"File {fileName} caused issue: {description}");
        public static void ThrowTreeParsingException(string description) =>
            throw new ParserEngineException($"Failed to parse tree: {description}");
    }
}
